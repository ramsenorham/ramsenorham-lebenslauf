<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the professional background information from the database
    $sql_professional_background = "SELECT `id`, `description`, `_from` AS `from`, `_to` AS `to` FROM `professional_background` ORDER BY `id` DESC;";
    // Execute the SQL query
    $result_professional_background = mysqli_query($conn, $sql_professional_background);
    
    // Check if the query was successful
    if (!$result_professional_background) {
      echo "Could not successfully run query ($sql_professional_background) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $professional_background = mysqli_fetch_all($result_professional_background, MYSQLI_ASSOC);

    // Return the professional background information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($professional_background, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>