<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the work experience information from the database
    $sql_qualifications = "SELECT `description`, `progression` FROM `qualification`;";
    // Execute the SQL query
    $result_qualifications = mysqli_query($conn, $sql_qualifications);
    
    // Check if the query was successful
    if (!$result_qualifications) {
      echo "Could not successfully run query ($sql_qualifications) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $qualifications = mysqli_fetch_all($result_qualifications, MYSQLI_ASSOC);

    // Return the work experience information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($qualifications, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>