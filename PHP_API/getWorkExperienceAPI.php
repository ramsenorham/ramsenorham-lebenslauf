<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the work experience information from the database
    $sql_work_experience = "SELECT learn.description, _from AS 'from', _to AS 'to' FROM learn, learn_type  WHERE learn.l_t_id = learn_type.id AND learn_type.description LIKE 'Fortbildung' ORDER BY _from DESC;";
    // Execute the SQL query
    $result_work_experience = mysqli_query($conn, $sql_work_experience);
    
    // Check if the query was successful
    if (!$result_work_experience) {
      echo "Could not successfully run query ($sql_work_experience) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $work_experience = mysqli_fetch_all($result_work_experience, MYSQLI_ASSOC);

    // Return the work experience information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($work_experience, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>