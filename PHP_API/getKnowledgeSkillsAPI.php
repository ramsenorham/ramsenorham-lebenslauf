<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the work experience information from the database
    $sql_knowledge_skills = "SELECT `skill`, `description` FROM `knowledge_skills`;";
    // Execute the SQL query
    $result_knowledge_skills = mysqli_query($conn, $sql_knowledge_skills);
    
    // Check if the query was successful
    if (!$result_knowledge_skills) {
      echo "Could not successfully run query ($sql_knowledge_skills) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $knowledge_skills = mysqli_fetch_all($result_knowledge_skills, MYSQLI_ASSOC);

    // Return the work experience information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($knowledge_skills, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>