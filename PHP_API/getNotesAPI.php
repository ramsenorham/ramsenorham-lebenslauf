<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    // Extract the note parameter from the GET request
    $note = $_GET['note'];

    // Get the work experience information from the database
    $sql_notes = "SELECT `_date` AS `date`, `notes` AS `notes` FROM `special` WHERE `l_id`= $note;";
    // Execute the SQL query
    $result_notes = mysqli_query($conn, $sql_notes);
    
    // Check if the query was successful
    if (!$result_notes) {
      echo "Could not successfully run query ($sql_notes) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $notes = mysqli_fetch_all($result_notes, MYSQLI_ASSOC);

    // Return the work experience information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($notes, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>
