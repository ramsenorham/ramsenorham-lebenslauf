<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the personal information from the database
    $sql = "SELECT CONCAT('(',title, ') ',firstname, ' ',lastname) AS fullname, CONCAT(DATE_FORMAT(dateofbirth,'%d.%m.%Y'), ' in ', placeofbirth) AS date_placeofbirth, phone, email, address, maritalstatus, nationality, nationalityType, gender FROM personal_information WHERE 1;";
    // SQL-Abfrage ausführen
    $result = mysqli_query($conn, $sql);
    // zum testen
    if (!$result) {
      echo "Could not successfully run query ($sql) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    $personal_info = mysqli_fetch_assoc($result);
  
    // Return the personal information and professional background as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($personal_info, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>