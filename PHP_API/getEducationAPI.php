<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the Education information from the database
    $sql_education = "SELECT learn.id, learn.description, _from AS 'from', _to AS 'to' FROM learn, learn_type  WHERE learn.l_t_id = learn_type.id AND learn_type.description LIKE 'Studium' ORDER BY _from DESC;";
    // Execute the SQL query
    $result_education = mysqli_query($conn, $sql_education);
    
    // Check if the query was successful
    if (!$result_education) {
      echo "Could not successfully run query ($sql_education) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $education = mysqli_fetch_all($result_education, MYSQLI_ASSOC);

    // Return the professional background information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($education, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>