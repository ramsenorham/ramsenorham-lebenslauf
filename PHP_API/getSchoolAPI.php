<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the work experience information from the database
    $sql_school = "SELECT learn.description, _from AS 'from', _to AS 'to' FROM learn, learn_type  WHERE learn.l_t_id = learn_type.id AND learn_type.description LIKE 'Schule' ORDER BY _from DESC;";
    // Execute the SQL query
    $result_school = mysqli_query($conn, $sql_school);
    
    // Check if the query was successful
    if (!$result_school) {
      echo "Could not successfully run query ($sql_school) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $school = mysqli_fetch_all($result_school, MYSQLI_ASSOC);

    // Return the work experience information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($school, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>