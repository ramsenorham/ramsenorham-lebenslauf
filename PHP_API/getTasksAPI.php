<?php
// Connect to the database
include "../dbConfig.php";

// Verbindung zur Datenbank herstellen
$conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['dbname']);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the HTTP request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    // Extract the task parameter from the GET request
    $task = $_GET['task'];

    // Get the work experience information from the database
    $sql_tasks = "SELECT `description` FROM `tasks` WHERE `pro_backg_id`= $task;";
    // Execute the SQL query
    $result_tasks = mysqli_query($conn, $sql_tasks);
    
    // Check if the query was successful
    if (!$result_tasks) {
      echo "Could not successfully run query ($sql_tasks) from DB: " . mysqli_error($conn);
      exit;
    } 
    
    // Fetch all rows of the result as an associative array
    $tasks = mysqli_fetch_all($result_tasks, MYSQLI_ASSOC);

    // Return the work experience information as JSON
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($tasks, JSON_PRETTY_PRINT);
}

// Close the database connection
mysqli_close($conn);
?>
