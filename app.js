function sayHello() {
    console.log("Hello World");
}

const api = "PHP_API/getPersonalInfoAPI.php";

async function getPersonalInfo() {
    try {
        const response = await fetch(api);
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const personal_info = await response.json();
        console.log(personal_info);
        // Update the HTML with the personal information
        document.querySelector('#fullname').textContent = personal_info.fullname;
        document.querySelector('#address').textContent = personal_info.address;
        document.querySelector('#DateofBirth_Place').textContent = personal_info.date_placeofbirth;
        document.querySelector('#mobil').textContent = personal_info.phone;
        document.querySelector('#email').textContent = personal_info.email;
        document.querySelector('#marital_status').textContent = personal_info.maritalstatus;
        document.querySelector('#nationality').textContent = personal_info.nationality;
        document.querySelector('#nationality_type').textContent = personal_info.nationalityType;
        document.querySelector('#gender').textContent = personal_info.gender;
    } catch (error) {
        console.error(error);
    }
}

async function getProfessionalBackground() {
    try {
        const response = await fetch('PHP_API/getProfessionalBackgroundAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var professionalBackgroundSection = document.getElementById("professional_background");
        var professionalBackgroundList = document.createElement("ul");

        data.forEach(async function(item) {
            var professionalBackgroundItem = document.createElement("li");

            var professionalBackgroundDiv = document.createElement("div");
            professionalBackgroundDiv.className = "container";

            var calendarIcon = document.createElement("i");
            calendarIcon.className = "fa fa-calendar fa-lg";
            professionalBackgroundDiv.appendChild(calendarIcon);

            var professionalBackgroundDate = document.createElement("h3");

            var fromDate = new Date(item.from);
            var formattedFromDate = fromDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });
            var toDate = new Date(item.to);
            var formattedToDate = toDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });

            professionalBackgroundDate.innerHTML = formattedFromDate + " - " + formattedToDate;

            professionalBackgroundDiv.appendChild(professionalBackgroundDate);

            professionalBackgroundItem.appendChild(professionalBackgroundDiv);


            var professionalBackgroundDescription = document.createElement("p");
            professionalBackgroundDescription.innerHTML = item.description;
            professionalBackgroundItem.appendChild(professionalBackgroundDescription);

            var professionalBackgroundTasks = document.createElement("ul");

            const tasksData = await getTask(item.id);
            tasksData.forEach(function(task) {
                var professionalBackgroundTaskItem = document.createElement("li");
                var professionalBackgroundTaskParagraph = document.createElement("p");
                professionalBackgroundTaskParagraph.innerHTML = task.description; // Display task description
                professionalBackgroundTaskItem.appendChild(professionalBackgroundTaskParagraph);
                professionalBackgroundTasks.appendChild(professionalBackgroundTaskItem);
            });
            professionalBackgroundItem.appendChild(professionalBackgroundTasks);
            professionalBackgroundList.appendChild(professionalBackgroundItem);
        });

        professionalBackgroundSection.appendChild(professionalBackgroundList);

    } catch (error) {
        console.error(error);
    }
}

async function getTask(task) {
    let data = null;

    // Define the API URL with the parameter
    var api_url = "PHP_API/getTasksAPI.php?task=" + task;
    try {
        // Fetch the data from the API
        const response = await fetch(api_url);
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        data = await response.json();
        console.log(data);

    } catch (error) {
        console.error(error);
    }
    return data;
}

async function getWorkExperience() {
    try {
        const response = await fetch('PHP_API/getWorkExperienceAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var WorkExperienceSection = document.getElementById("work_experience");
        var WorkExperienceList = document.createElement("ul");

        data.forEach(function(item) {
            var WorkExperienceItem = document.createElement("li");

            var WorkExperienceDiv = document.createElement("div");
            WorkExperienceDiv.className = "container";

            var calendarIcon = document.createElement("i");
            calendarIcon.className = "fa fa-calendar fa-lg";
            WorkExperienceDiv.appendChild(calendarIcon);

            var WorkExperienceDate = document.createElement("h3");

            var fromDate = new Date(item.from);
            var formattedFromDate = fromDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });
            var toDate = new Date(item.to);
            var formattedToDate = toDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });

            WorkExperienceDate.innerHTML = formattedFromDate + " - " + formattedToDate;
            WorkExperienceDiv.appendChild(WorkExperienceDate);

            WorkExperienceItem.appendChild(WorkExperienceDiv);

            var WorkExperienceDescription = document.createElement("p");
            WorkExperienceDescription.innerHTML = item.description;
            WorkExperienceItem.appendChild(WorkExperienceDescription);

            WorkExperienceList.appendChild(WorkExperienceItem);
        });

        WorkExperienceSection.appendChild(WorkExperienceList);

    } catch (error) {
        console.error(error);
    }
};

async function getEducation() {
    try {
        const response = await fetch('PHP_API/getEducationAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var educationSection = document.getElementById("education");
        var educationList = document.createElement("ul");

        data.forEach(async function(item) {
            var educationItem = document.createElement("li");

            var educationDiv = document.createElement("div");
            educationDiv.className = "container";

            var calendarIcon = document.createElement("i");
            calendarIcon.className = "fa fa-calendar fa-lg";
            educationDiv.appendChild(calendarIcon);

            var educationDate = document.createElement("h3");

            var fromDate = new Date(item.from);
            var formattedFromDate = fromDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });
            var toDate = new Date(item.to);
            var formattedToDate = toDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });

            educationDate.innerHTML = formattedFromDate + " - " + formattedToDate;
            educationDiv.appendChild(educationDate);

            educationItem.appendChild(educationDiv);

            var educationDescription = document.createElement("p");
            educationDescription.innerHTML = item.description;
            educationItem.appendChild(educationDescription);

            var educationNotes = document.createElement("ul");
            const notesData = await getNotes(item.id);
            notesData.forEach(function(notes) {
                var educationNotesItem = document.createElement("li");

                var educationNotesDiv = document.createElement("div");
                educationNotesDiv.className = "container";

                var calendarIcon = document.createElement("i");
                calendarIcon.className = "fa fa-calendar fa-lg";

                educationNotesDiv.appendChild(calendarIcon);

                var educationNotesParagraph = document.createElement("p");
                educationNotesParagraph.innerHTML = notes.date + ": " + notes.notes;
                educationNotesDiv.appendChild(educationNotesParagraph);

                educationNotesItem.appendChild(educationNotesDiv);
                educationNotes.appendChild(educationNotesItem);
            });
            educationItem.appendChild(educationNotes);

            educationList.appendChild(educationItem);
        });

        educationSection.appendChild(educationList);

    } catch (error) {
        console.error(error);
    }
};

async function getNotes(lern) {
    let data = null;

    // Define the API URL with the parameter
    var api_url = "PHP_API/getNotesAPI.php?note=" + lern;
    try {
        // Fetch the data from the API
        const response = await fetch(api_url);
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        data = await response.json();
        console.log(data);

    } catch (error) {
        console.error(error);
    }
    return data;
}

async function getSchool() {
    try {
        const response = await fetch('PHP_API/getSchoolAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var schoolSection = document.getElementById("school");
        var schoolList = document.createElement("ul");

        data.forEach(function(item) {
            var schoolItem = document.createElement("li");

            var schoolDiv = document.createElement("div");
            schoolDiv.className = "container";

            var calendarIcon = document.createElement("i");
            calendarIcon.className = "fa fa-calendar fa-lg";

            schoolDiv.appendChild(calendarIcon);

            var schoolDate = document.createElement("h3");

            var fromDate = new Date(item.from);
            var formattedFromDate = fromDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });
            var toDate = new Date(item.to);
            var formattedToDate = toDate.toLocaleDateString('de-DE', { month: '2-digit', year: 'numeric' });

            schoolDate.innerHTML = formattedFromDate + " - " + formattedToDate;
            schoolDiv.appendChild(schoolDate);

            schoolItem.appendChild(schoolDiv);

            var schoolDescription = document.createElement("p");
            schoolDescription.innerHTML = item.description;
            schoolItem.appendChild(schoolDescription);

            schoolList.appendChild(schoolItem);
        });

        schoolSection.appendChild(schoolList);

    } catch (error) {
        console.error(error);
    }
};

async function getQualifications() {
    try {
        const response = await fetch('PHP_API/getQualificationsAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var qualificationsSection = document.getElementById("qualifications");
        var qualificationsList = document.createElement("ul");

        data.forEach(function(item) {
            var qualificationsItem = document.createElement("li");

            var qualificationsDescription = document.createElement("div");
            qualificationsDescription.innerHTML = item.description;

            var qualificationsDiv = document.createElement("div");
            qualificationsDiv.className = "barhoriz";

            var progressDiv = document.createElement("div");
            progressDiv.className = "hprogress hprogress" + Math.floor(item.progression) + " bargreen";
            progressDiv.innerHTML = Math.floor(item.progression) + " %";
            qualificationsDiv.appendChild(progressDiv);

            qualificationsDescription.appendChild(qualificationsDiv);

            qualificationsItem.appendChild(qualificationsDescription);

            qualificationsList.appendChild(qualificationsItem);
        });

        qualificationsSection.appendChild(qualificationsList);

    } catch (error) {
        console.error(error);
    }
};

async function getKnowledgeSkills() {
    try {
        const response = await fetch('PHP_API/getKnowledgeSkillsAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var knowledgeSkillsSection = document.getElementById("knowledge_skills");

        var knowledgeSkillsHeader = document.createElement("p");
        var knowledgeSkillsCategories = {};

        data.forEach(function(item) {
            if (knowledgeSkillsCategories[item.skill]) {
                knowledgeSkillsCategories[item.skill].push(item.description);
            } else {
                knowledgeSkillsCategories[item.skill] = [item.description];
            }
        });

        for (var category in knowledgeSkillsCategories) {
            var categoryHeader = document.createElement("p");
            categoryHeader.innerHTML = category + ": ";

            var categorySkills = knowledgeSkillsCategories[category];
            categorySkills.forEach(function(skill, index) {

                if (index === categorySkills.length - 1) {
                    categoryHeader.innerHTML += skill + ".";
                } else {
                    categoryHeader.innerHTML += skill + ", ";
                }

            });

            knowledgeSkillsHeader.appendChild(categoryHeader);
        }

        knowledgeSkillsSection.appendChild(knowledgeSkillsHeader);
    } catch (error) {
        console.error(error);
    }
};

async function getSkills() {
    try {
        const response = await fetch('PHP_API/getSkillsAPI.php');
        if (!response.ok) {
            throw new Error(`HTTP error, status = ${response.status}`);
        }
        const data = await response.json();
        console.log(data);
        var skillsSection = document.getElementById("skills");
        var skillsHeader = document.createElement("p");
        var descriptions = "";
        data.forEach(function(item, index) {
            descriptions += item.description;
            if (index < data.length - 1) {
                descriptions += ", ";
            } else {
                descriptions += ".";
            }
        });
        skillsHeader.innerHTML = descriptions;
        skillsSection.appendChild(skillsHeader);
    } catch (error) {
        console.error(error);
    }
};

getPersonalInfo();
getProfessionalBackground();
getWorkExperience();
getEducation();
getSchool();
getQualifications();
getKnowledgeSkills();
getSkills();