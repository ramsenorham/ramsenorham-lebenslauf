-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 27. Jun 2023 um 14:09
-- Server-Version: 8.0.33-0ubuntu0.20.04.2
-- PHP-Version: 7.4.3-4ubuntu2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ramsenorham_lebenslauf`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `interessen`
--

CREATE TABLE `interessen` (
  `id` int NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `per_info_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `interessen`
--

INSERT INTO `interessen` (`id`, `description`, `notes`, `per_info_id`) VALUES
(1, 'Schwimmen', NULL, 1),
(2, 'Keyboard spielen', NULL, 1),
(3, 'Arbeiten mit dem Computer', NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `knowledge_skills`
--

CREATE TABLE `knowledge_skills` (
  `id` int NOT NULL,
  `skill` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `per_info_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `knowledge_skills`
--

INSERT INTO `knowledge_skills` (`id`, `skill`, `description`, `per_info_id`) VALUES
(1, 'Muttersprache', 'Aramäisch', 1),
(2, 'Muttersprache', 'Arabisch', 1),
(3, 'Weitere Sprache', 'Deutsch - C1', 1),
(4, 'Weitere Sprache', 'Englisch - fortgeschrittene Kenntnisse', 1),
(5, 'Führerschein', 'Klasse B', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `learn`
--

CREATE TABLE `learn` (
  `id` int NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `_from` date DEFAULT NULL,
  `_to` date DEFAULT NULL,
  `per_info_id` int DEFAULT NULL,
  `l_t_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `learn`
--

INSERT INTO `learn` (`id`, `description`, `_from`, `_to`, `per_info_id`, `l_t_id`) VALUES
(1, 'Grundschule Tal-Tamer, Al-Hasaka (Syrien).', '1994-09-04', '2000-05-31', 1, 2),
(2, 'Realschule Tal-Tamer, Al-Hasaka (Syrien).', '2001-09-04', '2003-05-31', 1, 2),
(3, 'Gymnasium Tal-Tamer, Al-Hasaka (Syrien), Erfolgreicher Abschluss mit Abitur.', '2004-09-04', '2006-06-30', 1, 2),
(4, 'Mamoun Private Universität für Wissenschaft und Technologie, in Qamishli, Syrien, erfolgreicher Abschluss Bachelor Computer System Engineer.', '2007-09-03', '2013-01-31', 1, 1),
(5, 'Integrationskurs an der Bildungskolleg Schule in Weilheim, gefördert durch die Agentur für Arbeit, Abschluss TELC B1.', '2014-04-01', '2015-04-30', 1, 3),
(6, 'Deutschkurs bei der ASL-Schule in München, gefördert durch die Otto Benecke Stiftung e.V. Bonn, Abschluss B2.2.', '2015-07-01', '2016-03-31', 1, 3),
(7, 'B.I.B.  Weilheim, Berufspraktische Weiterbildung für ausländische Arbeitnehmer, gefördert durch die Agentur für Arbeit.', '2018-03-01', '2018-06-29', 1, 3),
(8, 'Deutsch für den Beruf - für Fach- und Führungskräfte bei Alfatraining in München, gefördert durch die Agentur für Arbeit, Abschluss C1 WiDaF.', '2019-02-01', '2019-03-29', 1, 3),
(9, 'Relationale Datenbanken – SQL bei Alfatraining in München, gefördert durch die Agentur für Arbeit.', '2019-04-01', '2019-04-30', 1, 3),
(10, 'Oracle Datenbankadministration bei Alfatraining in München, gefördert durch die Agentur für Arbeit.', '2019-05-01', '2019-05-31', 1, 3),
(11, 'JAVA – SE 12 Objektorientierte Programmierung bei Alfatraining in München, gefördert durch die Agentur für Arbeit.', '2019-07-01', '2019-08-30', 1, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `learn_type`
--

CREATE TABLE `learn_type` (
  `id` int NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `notes` text COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `learn_type`
--

INSERT INTO `learn_type` (`id`, `description`, `notes`) VALUES
(1, 'Studium', NULL),
(2, 'Schule', NULL),
(3, 'Fortbildung', NULL),
(4, 'Ausbildung', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `personal_information`
--

CREATE TABLE `personal_information` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `dateofbirth` date NOT NULL,
  `placeofbirth` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `maritalstatus` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `nationalityType` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `personal_information`
--

INSERT INTO `personal_information` (`id`, `title`, `firstname`, `lastname`, `dateofbirth`, `placeofbirth`, `phone`, `email`, `address`, `maritalstatus`, `nationality`, `nationalityType`, `gender`) VALUES
(1, 'Ing.', 'Ramsen', 'Orham', '1988-08-07', 'Al-Hasaka, Syrien', '01745803055', 'ramsenorham@gmail.com', 'Singoldstraße 6, 86165 Augsburg', 'kirchlich verheiratet', 'syrisch, mit einem deutschen Aufenthaltstitel', 'Aufenthaltserlaubnis, Erwerbstätigkeitserlaubnis', 'männlich');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `professional_background`
--

CREATE TABLE `professional_background` (
  `id` int NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `_from` date NOT NULL,
  `_to` date NOT NULL,
  `per_info_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `professional_background`
--

INSERT INTO `professional_background` (`id`, `description`, `_from`, `_to`, `per_info_id`) VALUES
(1, 'Praktikum als Programmierer des ERP-Systems (Schröder Maschinenbau GmbH) in Wessobrunn-Forst', '2016-03-01', '2016-04-01', 1),
(2, 'Software-Entwicklung (K&L GmbH & Co.Handels-KG) in Weilheim i.OB', '2016-08-01', '2018-01-31', 1),
(3, 'Praktikum (Bayerische Landesbank) in München', '2018-07-01', '2018-12-31', 1),
(4, 'Berufsausbildung Fachinformatiker für Anwendungsentwicklung (BinaStar GmbH) in Weilheim i.OB', '2019-09-02', '2022-07-29', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `qualification`
--

CREATE TABLE `qualification` (
  `id` int NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_general_ci,
  `per_info_id` int DEFAULT NULL,
  `progression` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `qualification`
--

INSERT INTO `qualification` (`id`, `description`, `notes`, `per_info_id`, `progression`) VALUES
(1, 'Frontend-Entwicklung: JavaFX, HTML, CSS, JavaScript, JSON, WordPress und Mapbox.', NULL, 1, '60.00'),
(2, 'Linux DevOps Tools SVN, Git, Apache Tomcat und NGINX.', NULL, 1, '60.00'),
(3, 'Programmierung mit Java, SQL, T-SQL und PL/SQL.', NULL, 1, '70.00'),
(4, 'Datenbanken erstellen MySQL und Microsoft SQL Server.', NULL, 1, '80.00'),
(5, 'Datenbanken Administrator Oracle.', NULL, 1, '50.00'),
(6, 'iReport, JasperReports und Bildbearbeitung mit Adobe Photoshop.', NULL, 1, '80.00'),
(7, 'Microsoft Office (Word, Excel, Powerpoint).', NULL, 1, '80.00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `special`
--

CREATE TABLE `special` (
  `id` int NOT NULL,
  `_date` date NOT NULL,
  `notes` text COLLATE utf8mb4_general_ci NOT NULL,
  `l_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `special`
--

INSERT INTO `special` (`id`, `_date`, `notes`, `l_id`) VALUES
(1, '2017-10-02', 'Zeugnis Bewertung von der ZAB erhalten.\r\nDer ausländische Abschluss entspricht einem deutschen Hochschulabschluss auf Bachelor-Ebene.', 4),
(2, '2018-09-03', 'Anerkennung als Ingenieur.\r\nGenehmigung zum Führen der Berufsbezeichnung Ingenieur von der Regierung von Schwaben erhalten.', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tasks`
--

CREATE TABLE `tasks` (
  `id` int NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `notes` text COLLATE utf8mb4_general_ci,
  `pro_backg_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tasks`
--

INSERT INTO `tasks` (`id`, `description`, `notes`, `pro_backg_id`) VALUES
(1, 'Programmieren in der Entwicklungsumgebung Easycode.', '', 1),
(2, 'Formulardesign mit iReport Designer.', '', 1),
(3, 'Maskenanpassungen mit abas-ERP Screen Editor.', '', 1),
(4, 'Anlage von Stammdaten in abas-ERP.', '', 1),
(5, 'Aufbauen von Kenntnissen in SQL.', '', 2),
(6, 'Erlernen des sicheren Umgangs mit JasperReports, einem Reporting- Werkzeug.', '', 2),
(7, 'Mitarbeit bei unserem Migrationsprojekt (Ablösung von OracleReports).', '', 2),
(8, 'Erstellung von neu angeforderten Berichten mit JasperReports.', '', 2),
(9, 'Einarbeiten in PL/SQL.', '', 2),
(10, 'Programmieren kleiner Prozeduren und Funktionen mit PL/SQL, vor allem Entwicklung neuer Berichte zu unterstützen.', '', 2),
(11, 'Einarbeitung in die Funktionen des ETL Entwicklertool SAS data integration Studio.', '', 3),
(12, 'Erfolgreich durchgeführte Einführungsschulung im SAS enterprise Guide.', '', 3),
(13, 'Neuprogrammierung und Anpassung von ETL-Programmstrecken am Kundenrelevanten Tages-Informationssystem Kurti für Financial Markets.', '', 3),
(14, 'LoanIQ-Host-Batch Monitoring, JIRA Workflow System, Mainframe Tools (TSO, TWS, Beta).', '', 3),
(15, 'Vertiefung der C++ Kenntnisse an praxisnahen Beispielen im Entwicklungsbereich des Handels-positionsführungssystem Summit.', '', 3),
(16, 'Aufbauen von Kenntnissen in Linux command line.', '', 4),
(17, 'Erlernen des sicheren Umgangs mit Mapbox.', '', 4),
(18, 'Einarbeiten in HTML, JavaScript und CSS.', '', 4),
(19, 'Frontend Entwicklung Projekte (Webdesign und Webprogrammierung auf Linux).', '', 4);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `interessen`
--
ALTER TABLE `interessen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `per_info_id` (`per_info_id`);

--
-- Indizes für die Tabelle `knowledge_skills`
--
ALTER TABLE `knowledge_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `per_info_id` (`per_info_id`);

--
-- Indizes für die Tabelle `learn`
--
ALTER TABLE `learn`
  ADD PRIMARY KEY (`id`),
  ADD KEY `per_info_id` (`per_info_id`),
  ADD KEY `l_t_id` (`l_t_id`);

--
-- Indizes für die Tabelle `learn_type`
--
ALTER TABLE `learn_type`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `personal_information`
--
ALTER TABLE `personal_information`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `professional_background`
--
ALTER TABLE `professional_background`
  ADD PRIMARY KEY (`id`),
  ADD KEY `per_info_id` (`per_info_id`);

--
-- Indizes für die Tabelle `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `per_info_id` (`per_info_id`);

--
-- Indizes für die Tabelle `special`
--
ALTER TABLE `special`
  ADD PRIMARY KEY (`id`),
  ADD KEY `l_id` (`l_id`);

--
-- Indizes für die Tabelle `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pro_backg_id` (`pro_backg_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `interessen`
--
ALTER TABLE `interessen`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `knowledge_skills`
--
ALTER TABLE `knowledge_skills`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `learn`
--
ALTER TABLE `learn`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT für Tabelle `learn_type`
--
ALTER TABLE `learn_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `personal_information`
--
ALTER TABLE `personal_information`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `professional_background`
--
ALTER TABLE `professional_background`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `qualification`
--
ALTER TABLE `qualification`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `special`
--
ALTER TABLE `special`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `interessen`
--
ALTER TABLE `interessen`
  ADD CONSTRAINT `interessen_ibfk_1` FOREIGN KEY (`per_info_id`) REFERENCES `personal_information` (`id`);

--
-- Constraints der Tabelle `knowledge_skills`
--
ALTER TABLE `knowledge_skills`
  ADD CONSTRAINT `knowledge_skills_ibfk_1` FOREIGN KEY (`per_info_id`) REFERENCES `personal_information` (`id`);

--
-- Constraints der Tabelle `learn`
--
ALTER TABLE `learn`
  ADD CONSTRAINT `learn_ibfk_1` FOREIGN KEY (`per_info_id`) REFERENCES `personal_information` (`id`),
  ADD CONSTRAINT `learn_ibfk_2` FOREIGN KEY (`l_t_id`) REFERENCES `learn_type` (`id`);

--
-- Constraints der Tabelle `professional_background`
--
ALTER TABLE `professional_background`
  ADD CONSTRAINT `professional_background_ibfk_1` FOREIGN KEY (`per_info_id`) REFERENCES `personal_information` (`id`);

--
-- Constraints der Tabelle `qualification`
--
ALTER TABLE `qualification`
  ADD CONSTRAINT `qualification_ibfk_1` FOREIGN KEY (`per_info_id`) REFERENCES `personal_information` (`id`);

--
-- Constraints der Tabelle `special`
--
ALTER TABLE `special`
  ADD CONSTRAINT `special_ibfk_1` FOREIGN KEY (`l_id`) REFERENCES `learn` (`id`);

--
-- Constraints der Tabelle `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`pro_backg_id`) REFERENCES `professional_background` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
